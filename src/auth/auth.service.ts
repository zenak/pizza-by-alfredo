import { Injectable } from '@nestjs/common';
import { CustomerManager } from 'src/customer/customer.service';
import { JwtService } from '@nestjs/jwt';
import { Customer } from 'src/customer/model/customer.model';

@Injectable()
export class AuthService {
    constructor(
        private customerManager: CustomerManager,
        private jwtService: JwtService,
    ) {}

    // validateUser = this.customerManager.authenticateUser;
    validateUser(email: string, password: string) {
        return this.customerManager.authenticateUser(email, password);
    }

    generateAccessToken(user: Customer) {
        const payload = { username: user.email, sub: user.id };
        return {
            access_token: this.jwtService.sign(payload),
        };
    }
}
