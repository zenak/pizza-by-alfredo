interface Customer {
    id: number;
    name: string;
    surname: string;
    email: string;
    password: string;
    loyaltyPoint: number;
}

export { Customer };
