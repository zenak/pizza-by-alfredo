import { Injectable } from '@nestjs/common';
import { Customer } from './model/customer.model';
import { CreateCustomerData } from './validation/createCustomer.validation';

@Injectable()
export class CustomerManager {
    private readonly customerData: Customer[] = [];

    register(customerData: CreateCustomerData) {
        const customer: Customer = {
            ...customerData,
            password: this.fakeHashPassword(customerData.password),
            id: this.customerData.length,
            loyaltyPoint: 0,
        };
        this.customerData.push(customer);
    }

    authenticateUser(email: string, password: string): Customer | undefined {
        const user = this.getCustomerByEmailAux(email);
        if (user && user.password === this.fakeHashPassword(password)) {
            return this.removePasswordFromCustomerData(user);
        }
    }

    getCustomerByEmail(email: string): Customer | undefined {
        const user = this.getCustomerByEmailAux(email);
        if (user) {
            return this.removePasswordFromCustomerData(user);
        }
    }

    getCustomerById(id: number): Customer | undefined {
        const user = this.customerData.find((customer) => {
            return customer.id === id;
        });
        if (user) {
            return this.removePasswordFromCustomerData(user);
        }
    }

    updateUserLoyaltyPoints(userId: number, points: number) {
        const customer = this.customerData.find((customer) => {
            return customer.id === userId;
        });
        (customer as Customer).loyaltyPoint += points;
    }

    doesEmailExists(email: string): boolean {
        return this.getCustomerByEmailAux(email) !== undefined;
    }

    getAll() {
        return this.customerData;
    }

    private fakeHashPassword(password: string): string {
        return password;
    }

    private getCustomerByEmailAux(email: string): Customer | undefined {
        return this.customerData.find((customer) => {
            return customer.email === email;
        });
    }

    private removePasswordFromCustomerData(user: Customer): Customer {
        return { ...user, password: '' };
    }
}
