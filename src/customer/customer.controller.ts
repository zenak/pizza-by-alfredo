import {
    Controller,
    Post,
    Get,
    Request,
    Body,
    UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { CustomerManager } from './customer.service';
import { CreateCustomerData } from './validation/createCustomer.validation';

@Controller('customer')
export class CustomerController {
    constructor(private customerManager: CustomerManager) {}

    @Post()
    register(@Body() customer: CreateCustomerData) {
        this.customerManager.register(customer);
    }

    @UseGuards(AuthGuard('jwt'))
    @Get()
    getCustomer(@Request() req: any) {
        return this.customerManager.getCustomerByEmail(req.user.email);
    }

    // TODO delete me
    @Get('all')
    getAllCustomers() {
        return this.customerManager.getAll();
    }
}
