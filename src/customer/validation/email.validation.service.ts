import { Injectable } from '@nestjs/common';
import {
    ValidatorConstraintInterface,
    ValidatorConstraint,
    registerDecorator,
    ValidationOptions,
} from 'class-validator';
import { CustomerManager } from '../customer.service';

@ValidatorConstraint()
@Injectable()
export class UniqueEmailConstraintValidator
    implements ValidatorConstraintInterface
{
    constructor(private customerManager: CustomerManager) {}

    validate(email: string) {
        return !this.customerManager.doesEmailExists(email);
    }
}

export function UniqueEmail(validationOption?: ValidationOptions) {
    // eslint-disable-next-line @typescript-eslint/ban-types
    return function (object: Object, propertyName: string) {
        registerDecorator({
            target: object.constructor,
            propertyName: propertyName,
            options: validationOption,
            constraints: [],
            validator: UniqueEmailConstraintValidator,
        });
    };
}
