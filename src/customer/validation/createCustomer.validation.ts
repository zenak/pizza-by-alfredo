import { IsEmail, IsNotEmpty, IsString, MinLength } from 'class-validator';
import { UniqueEmail } from './email.validation.service';

export class CreateCustomerData {
    @IsString()
    @IsNotEmpty()
    name: string;

    @IsString()
    @IsNotEmpty()
    surname: string;

    @IsEmail()
    @UniqueEmail({
        message: 'This email address is already in use',
    })
    email: string;

    @IsString()
    @MinLength(6)
    password: string;
}
