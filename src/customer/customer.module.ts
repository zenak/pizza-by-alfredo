import { Module } from '@nestjs/common';
import { CustomerController } from './customer.controller';
import { CustomerManager } from './customer.service';
import { UniqueEmailConstraintValidator } from './validation/email.validation.service';

@Module({
    controllers: [CustomerController],
    providers: [CustomerManager, UniqueEmailConstraintValidator],
    exports: [CustomerManager],
})
export class CustomerModule {}
