export interface Observer<T> {
    notify(object: T): any;
}
