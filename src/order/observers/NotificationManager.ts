import { Injectable } from '@nestjs/common';
import { Observer } from './generic.observer';
import { Order } from '../model/order/order.interface';
import { CustomerManager } from 'src/customer/customer.service';
import { OrderManager } from '../order.service';

@Injectable()
export class NotificationManager implements Observer<Order> {
    constructor(
        orderManager: OrderManager,
        private customerManager: CustomerManager,
    ) {
        orderManager.registerObserver(this);
    }

    notify(order: Order) {
        const user = this.customerManager.getCustomerById(order.customerId);
        console.log('Send order confirm email to ' + user?.email);
    }
}
