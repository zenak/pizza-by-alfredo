import { Injectable } from '@nestjs/common';
import { Observer } from './generic.observer';
import { Order } from '../model/order/order.interface';
import { CustomerManager } from 'src/customer/customer.service';
import { OrderManager } from '../order.service';

@Injectable()
export class LoyaltyManager implements Observer<Order> {
    constructor(
        orderManager: OrderManager,
        private customerManager: CustomerManager,
    ) {
        orderManager.registerObserver(this);
    }

    notify(order: Order) {
        const loyaltyPoints = Math.floor(order.totalCost);
        this.customerManager.updateUserLoyaltyPoints(
            order.customerId,
            loyaltyPoints,
        );
    }
}
