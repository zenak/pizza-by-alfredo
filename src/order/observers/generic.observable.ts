import { Observer } from './generic.observer';

export interface Observable<T> {
    registerObserver(observer: Observer<T>): any;

    notifyObserver(order: T): any;
}
