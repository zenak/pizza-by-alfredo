import { BadRequestException, Injectable } from '@nestjs/common';
import { Basket } from './model/order/basket.interface';
import { Order } from './model/order/order.interface';
import { Pizza } from './model/pizza/pizza';
import { PizzaType } from './model/pizza/pizza.types';
import { PizzaFactory } from './model/pizza/pizzaFactory';
import { Topping } from './model/topping/topping';
import { ToppingType } from './model/topping/topping.type';
import { Observable } from './observers/generic.observable';
import { Observer } from './observers/generic.observer';

@Injectable()
export class OrderManager implements Observable<Order> {
    constructor() {
        // TODO avoid creating this coupling between Pizza class and this class
        Pizza.getToppingPrice = (type: ToppingType) => {
            const toppingData = this.toppingsData.find((topping) => {
                return topping.type === type;
            }) as Topping;
            return toppingData.cost;
        };
    }

    private baskets: Basket[] = [];
    private orders: Order[] = [];
    private pizzaFactory = new PizzaFactory();
    private toppingsData: Topping[] = [
        {
            type: ToppingType.FORMAGGIO,
            cost: 1.5,
            name: 'Formaggio',
            description: 'Extra cheese on your pizza',
        },
        {
            type: ToppingType.FUNGHI,
            cost: 2.5,
            name: 'Funghi',
            description: '',
        },
        {
            type: ToppingType.PATATE,
            cost: 2.0,
            name: 'Patate',
            description: '',
        },
    ];
    private observers: Observer<Order>[] = [];

    registerObserver(observer: Observer<Order>) {
        this.observers.push(observer);
    }

    // TODO make private
    notifyObserver(order: Order) {
        this.observers.forEach((observer) => {
            observer.notify(order);
        });
    }

    getCustomerBasket(customerId: number) {
        return this.baskets.find((basket) => {
            return customerId === basket.customerId;
        });
    }

    addPizzaToCustomerBasket(customerId: number, type: PizzaType) {
        const pizza = this.pizzaFactory.createPizza(type);
        const basket =
            this.getCustomerBasket(customerId) ||
            this.createBasketForCustomer(customerId);
        basket.items.push(pizza);
    }

    removePizzaFromCustomerBasket(customerId: number, pizzaIndex: number) {
        const basket = this.getExistingBasket(customerId);
        if (pizzaIndex >= basket.items.length) {
            throw new BadRequestException('Pizza not in Basket');
        }
        basket.items.splice(pizzaIndex, 1);
    }

    addToppingToPizza(
        customerId: number,
        pizzaIndex: number,
        type: ToppingType,
    ) {
        const basket = this.getExistingBasket(customerId);
        const pizza = this.getPizzaFromBasket(basket, pizzaIndex);
        pizza.addTopping(type);
    }

    removeToppingToPizza(
        customerId: number,
        pizzaIndex: number,
        type: ToppingType,
    ) {
        const basket = this.getExistingBasket(customerId);
        const pizza = this.getPizzaFromBasket(basket, pizzaIndex);
        pizza.removeTopping(type);
    }

    createOrder(customerId: number, paymentDetails: any, shippingDetails: any) {
        const basket = this.getExistingBasket(customerId);
        const totalCost = basket.items
            .map((item) => {
                return item.getCost();
            })
            .reduce((a, b) => {
                return a + b;
            }, 0);
        const order: Order = {
            customerId: customerId,
            items: basket.items,
            totalCost: totalCost,
            shippingDetails: shippingDetails,
            paymentDetails: paymentDetails,
        };
        this.orders.push(order);
        this.baskets.splice(this.baskets.indexOf(basket), 1);
        this.notifyObserver(order);
    }

    // TODO delete me
    getAllBaskets() {
        return this.baskets;
    }

    // TODO delete me
    getAllOrders() {
        return this.orders;
    }

    private createBasketForCustomer(customerId: number): Basket {
        const basket: Basket = {
            customerId: customerId,
            items: [],
        };
        this.baskets.push(basket);
        return basket;
    }

    private getExistingBasket(customerId: number): Basket {
        const basket = this.getCustomerBasket(customerId);
        if (!basket) {
            throw new BadRequestException('Basket not found');
        }
        return basket;
    }

    private getPizzaFromBasket(basket: Basket, pizzaIndex: number): Pizza {
        if (pizzaIndex >= basket.items.length) {
            throw new BadRequestException('Unable to find pizza');
        }
        return basket.items[pizzaIndex];
    }
}
