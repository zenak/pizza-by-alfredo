import { ToppingType } from '../topping/topping.type';
import { PizzaType } from './pizza.types';
import { BadRequestException, NotImplementedException } from '@nestjs/common';

export class Pizza {
    constructor(
        private readonly type: PizzaType,
        private readonly name: string,
        private readonly description: string,
        private readonly image: string,
        private readonly basePrice: number,
    ) {}

    private cost = this.basePrice;
    protected addedToppings: Set<ToppingType> = new Set();

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    static getToppingPrice(_topping: ToppingType): number {
        throw new NotImplementedException();
    }

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    protected canAddTopping(_topping: ToppingType): boolean {
        throw new NotImplementedException();
    }

    private updateCost() {
        this.cost = this.basePrice;
        this.addedToppings.forEach((topping) => {
            this.cost += Pizza.getToppingPrice(topping);
        });
    }

    addTopping(topping: ToppingType) {
        if (!this.canAddTopping(topping)) {
            throw new BadRequestException(
                'The selected topping cannot be added on this pizza',
            );
        }
        this.addedToppings.add(topping);
        this.updateCost();
    }

    removeTopping(topping: ToppingType) {
        this.addedToppings.delete(topping);
        this.updateCost();
    }

    getCost() {
        return this.cost;
    }
}
