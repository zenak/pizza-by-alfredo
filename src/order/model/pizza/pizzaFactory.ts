import { BadRequestException } from '@nestjs/common';
import { Pizza } from './pizza';
import { Diavola } from './pizza.diavola';
import { Margherita } from './pizza.margherita';
import { PizzaType } from './pizza.types';

export class PizzaFactory {
    private static instance: PizzaFactory;
    constructor() {
        if (PizzaFactory.instance) {
            return PizzaFactory.instance;
        }
        PizzaFactory.instance = this;
    }

    createPizza(type: PizzaType): Pizza {
        switch (type) {
            case PizzaType.MARGHERITA:
                return new Margherita();
            case PizzaType.DIAVOLA:
                return new Diavola();
            default:
                console.log(type);
                console.log(typeof type);
                throw new BadRequestException(
                    'Pizza type not present in the Pizzeria',
                );
        }
    }
}
