import { ToppingType } from '../topping/topping.type';
import { Pizza } from './pizza';
import { PizzaType } from './pizza.types';

export class Margherita extends Pizza {
    constructor() {
        // TODO get these values from a config file or DB
        super(PizzaType.MARGHERITA, 'Margherita', 'Pizza margherita', '', 5.0);
    }

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    protected canAddTopping(_topping: ToppingType): boolean {
        return this.addedToppings.size < 2;
    }
}
