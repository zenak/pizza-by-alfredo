import { ToppingType } from '../topping/topping.type';
import { Pizza } from './pizza';
import { PizzaType } from './pizza.types';

export class Diavola extends Pizza {
    constructor() {
        // TODO get these values from a config file or DB
        super(PizzaType.DIAVOLA, 'Diavola', 'Pizza diavola', '', 8.0);
    }

    protected canAddTopping(topping: ToppingType): boolean {
        return topping != ToppingType.PATATE;
    }
}
