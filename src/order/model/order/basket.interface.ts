import { Pizza } from '../pizza/pizza';

export interface Basket {
    customerId: number;
    items: Array<Pizza>;
}
