import { Pizza } from '../pizza/pizza';

export interface Order {
    customerId: number;
    items: Array<Pizza>;
    totalCost: number;
    shippingDetails: any;
    paymentDetails: any;
}
