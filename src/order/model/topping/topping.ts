import { ToppingType } from './topping.type';

export interface Topping {
    type: ToppingType;
    name: string;
    description: string;
    cost: number;
}
