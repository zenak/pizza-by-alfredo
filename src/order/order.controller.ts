import {
    Controller,
    Get,
    Post,
    Delete,
    Request,
    UseGuards,
    Body,
    Param,
    ParseIntPipe,
    Put,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { OrderManager } from './order.service';
import { PizzaTypeParam } from './validation/pizza.type.validation';
import { ToppingTypeParam } from './validation/topping.type.validation';

@Controller('basket')
@UseGuards(AuthGuard('jwt'))
export class OrderController {
    constructor(private orderManager: OrderManager) {}

    private getCustomerNumberFromRequest(req: any) {
        return req.user.customerId;
    }

    @Get()
    getBasket(@Request() req: any) {
        return this.orderManager.getCustomerBasket(req.user.customerId);
    }

    @Post('pizza')
    addPizzaToBasket(@Request() req: any, @Body() pizzaType: PizzaTypeParam) {
        this.orderManager.addPizzaToCustomerBasket(
            req.user.customerId,
            pizzaType.type,
        );
    }

    @Delete('pizza/:id')
    removePizzaFromBasket(
        @Request() req: any,
        @Param('id', ParseIntPipe) pizzaId: number,
    ) {
        this.orderManager.removePizzaFromCustomerBasket(
            req.user.customerId,
            pizzaId,
        );
    }

    @Put('pizza/:id/topping')
    addTopping(
        @Request() req: any,
        @Param('id', ParseIntPipe) pizzaId: number,
        @Body() toppingType: ToppingTypeParam,
    ) {
        this.orderManager.addToppingToPizza(
            req.user.customerId,
            pizzaId,
            toppingType.type,
        );
    }

    @Delete('pizza/:id/topping')
    removeTopping(
        @Request() req: any,
        @Param('id', ParseIntPipe) pizzaId: number,
        @Body() toppingType: ToppingTypeParam,
    ) {
        this.orderManager.removeToppingToPizza(
            req.user.customerId,
            pizzaId,
            toppingType.type,
        );
    }

    @Post('order')
    createOrder(@Request() req: any, @Body() body: any) {
        // TODO add Guard to check valid payment?
        this.orderManager.createOrder(
            req.user.customerId,
            body.paymentDetails,
            body.shippingDetails,
        );
    }

    // TODO delete me
    @Get('allBaskets')
    getAllBaskets() {
        return this.orderManager.getAllBaskets();
    }

    // TODO delete me!
    @Get('allOrders')
    getAllOrders() {
        return this.orderManager.getAllOrders();
    }
}
