import { Module } from '@nestjs/common';
import { CustomerModule } from 'src/customer/customer.module';
import { LoyaltyManager } from './observers/LoyaltyManager';
import { NotificationManager } from './observers/NotificationManager';
import { OrderController } from './order.controller';
import { OrderManager } from './order.service';

@Module({
    imports: [CustomerModule],
    controllers: [OrderController],
    providers: [OrderManager, NotificationManager, LoyaltyManager],
})
export class OrderModule {}
