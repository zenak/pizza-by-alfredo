import { IsEnum } from 'class-validator';
import { ToppingType } from '../model/topping/topping.type';

export class ToppingTypeParam {
    @IsEnum(ToppingType)
    type: ToppingType;
}
