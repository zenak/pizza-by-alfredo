import { IsEnum } from 'class-validator';
import { PizzaType } from '../model/pizza/pizza.types';

export class PizzaTypeParam {
    @IsEnum(PizzaType)
    type: PizzaType;
}
