import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CustomerModule } from './customer/customer.module';
import { AuthModule } from './auth/auth.module';
import { OrderModule } from './order/order.module';

@Module({
    imports: [CustomerModule, AuthModule, OrderModule],
    controllers: [AppController],
    providers: [AppService],
})
export class AppModule {}
